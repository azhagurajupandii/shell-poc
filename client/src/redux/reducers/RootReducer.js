import { combineReducers } from 'redux'
import viewTableReducer from './ViewTableReducer'

const appReducer = combineReducers({
    viewtable: viewTableReducer,
})

const RootReducer = (state, action) => {

  //to clera the state data
    if (action.type === 'LOGOUT') {
      state = {}
    }
  
    return appReducer(state, action)
  }

export default RootReducer
