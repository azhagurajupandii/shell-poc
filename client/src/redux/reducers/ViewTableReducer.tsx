import {
    VIEWTABLE, VIEWCSVDATA
} from '../actions/types'

const initialState = {
    tabledatas: [],
    csvdatas:[]
}

export default function (state = initialState, action: any) {
    switch (action.type) {
        case VIEWTABLE: {
            console.log("VIEWTABLE", action.payload);
            return {
                ...state,
                tabledatas: action.payload,
            }
        }
        case VIEWCSVDATA: {
            //console.log("VIEWCSVDATA", action.payload);
            return {
                ...state,
                csvdatas: action.payload,
            }
        }
        default:
            return state;
    }
}