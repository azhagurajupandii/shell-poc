import axios from 'axios'
import { VIEWTABLE,VIEWCSVDATA } from "../actions/types";

export const getTableList = () => (dispatch:any) => {
    console.log("inside getTableList action");
    axios.get('https://jsonplaceholder.typicode.com/todos').then((res) => {
        console.log("response api", res);
        dispatch({
            type: VIEWTABLE,
            payload: res.data,
        })
    })
}

export const getcSVTableList = () => (dispatch:any) => {
   // console.log("inside getcSVTableList action");
    axios.get('https://plotly.github.io/datasets/country_indicators.csv').then((res) => {
        console.log("CSV response api ", res);
        dispatch({
            type: VIEWCSVDATA,
            payload: res.data,
        })
    })
}
