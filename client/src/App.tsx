import React from 'react';
import logo from './asset/images/BH_Logo.png';
import './App.css';
import Table from './views/Table';
import { Provider } from 'react-redux';
import store from './redux/store';
import PlotlyChats from './views/PlotlyCharts';
import CrossChats from './views/CrossCharts';

function App() {
  return (
    <Provider store={store}>
      <div className="App" style={{ height: 400, width: '100%' }}>
        {/* <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <Table></Table>
        <PlotlyChats></PlotlyChats> */}
        <CrossChats></CrossChats>
      </div>
    </Provider>
  );
}

export default App;
