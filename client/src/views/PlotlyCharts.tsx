import React from 'react';
import Plot from 'react-plotly.js';

export default function PlotlyChats() {
    var trace1 = {
        x: ['South Korea', 'China', 'Canada'],
        y: [24, 10, 9],
        name: 'Gold',
        type: 'scatter',
        mode: 'markers'
    };

    var trace2 = {
        x: ['South Korea', 'China', 'Canada'],
        y: [13, 15, 12],
        name: 'Silver',
        type: 'scatter',
        mode: 'markers'
    };

    var trace3 = {
        x: ['South Korea', 'China', 'Canada'],
        y: [11, 8, 12],
        name: 'Bronze',
        type: 'scatter',
        mode: 'markers'
    };

    var groupdata: any = [trace1, trace2, trace3];

    var grouplayout: any = {
        scattermode: 'group',
        title: 'Grouped by Country',
        xaxis: { title: 'Country' },
        yaxis: { title: 'Medals' },
        scattergap: 0.7
    };

    return (
        <div>
            <h5>Plotly Charts</h5>
            <Plot data={groupdata} layout={grouplayout}></Plot>
        </div>
    )
}