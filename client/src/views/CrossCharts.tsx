import React, { useEffect } from 'react';
import Plot from 'react-plotly.js';
import { useSelector, useDispatch } from 'react-redux'
import { getcSVTableList } from '../redux/actions/ViewTableAction'
import Grid from '@mui/material/Grid';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';


export default function CrossChats() {
    const dispatch: any = useDispatch();
    useEffect(() => {
        dispatch(getcSVTableList(),
        );
        console.log()
    }, []);

    const crossplotcsvdata = useSelector(
        (state: any) => state.viewtable.csvdatas
    )
    //console.log("crossplotcsvdata", crossplotcsvdata);

    function csvJSON(text: string, quoteChar = '"', delimiter = ",") {
        var rows = text.split("\n");
        var headers = rows[0].split(",");

        const regex = new RegExp(
            `\\s*(${quoteChar})?(.*?)\\1\\s*(?:${delimiter}|$)`,
            "gs"
        );

        const match = (line: any) =>
            [...line.matchAll(regex)].map((m) => m[2]).slice(0, -1);

        var lines = text.split("\n");
        const heads = headers ?? match(lines.shift());
        lines = lines.slice(1);

        return lines.map((line: any) => {
            return match(line).reduce((acc, cur, i) => {
                // replace blank matches with `null`
                const val = cur.length <= 0 ? null : Number(cur) || cur;
                const key = heads[i] ?? `{i}`;
                return { ...acc, [key]: val };
            }, {});
        });
    }

    var csvtojson = csvJSON(crossplotcsvdata);
    console.log("Hi all csvtojson", csvtojson);

    const countries = csvtojson.reduce((acc: any, ele: any) => {
        if (acc.includes(ele.country)) {
            return acc;
        } else {
            return [...acc, ele.country];
        }
    }, []);

    console.log("Hi all countries", countries);


    const [age, setAge] = React.useState('');

    const handleChange = (event: SelectChangeEvent) => {
        setAge(event.target.value as string);
    };

    // if (crossplotcsvdata.length < 0) {

    //     var get= function (err: any, rows: any) {

    //         function unpack(rows: any, key: any) {
    //             return rows.map(function (row: any) { return row[key]; });
    //         }

    //         var allCountryNames = unpack(rows, 'country'),
    //             allYear = unpack(rows, 'year'),
    //             allGdp = unpack(rows, 'gdpPercap'),
    //             listofCountries = [],
    //             currentCountry,
    //             currentGdp = [],
    //             currentYear = [];

    //         for (var i = 0; i < allCountryNames.length; i++) {
    //             if (listofCountries.indexOf(allCountryNames[i]) === -1) {
    //                 listofCountries.push(allCountryNames[i]);
    //                 console.log("hi",allCountryNames);
    //             }
    //         }

    //     }
    // }
    // const [csvArray, setCsvArray] = React.useState([]);

    // const processCSV = (str:any, delim = ',') => {
    //     const headers = str.slice(0, str.indexOf('\n')).split(delim);
    //     const rows = str.slice(str.indexOf('\n') + 1).split('\n');

    //     console.log("headerss",headers)
    //     console.log("rowss",rows)

    //     const newArray = rows.map(row=> {
    //         const values = row.split(delim);
    //         const eachObject = headers.reduce((obj:any, header:any, i:any) => {
    //             obj[header] = values[i];
    //             return obj;
    //         }, {})
    //         return eachObject;
    //     })

    //     setCsvArray(crossplotcsvdata)
    // }

    return (
        <div>
            <h5>Cross Plot Charts</h5>
            <Grid container direction="row">
                <Grid item xs={5}>
                    <h5>X-Axis</h5>
                    <FormControl fullWidth>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={age}
                            label="Age"
                            onChange={handleChange}
                        >
                            <MenuItem value={10}>Ten</MenuItem>
                            <MenuItem value={20}>Twenty</MenuItem>
                            <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={1}></Grid>
                <Grid item xs={5}>
                    <h5>Y-Axis</h5>
                    <FormControl fullWidth>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={age}
                            label="Age"
                            onChange={handleChange}
                        >
                            <MenuItem value={10}>Ten</MenuItem>
                            <MenuItem value={20}>Twenty</MenuItem>
                            <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>
        </div>
    )
}